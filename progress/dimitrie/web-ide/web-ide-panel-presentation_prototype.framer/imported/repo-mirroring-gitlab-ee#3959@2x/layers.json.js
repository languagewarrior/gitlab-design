window.__imported__ = window.__imported__ || {};
window.__imported__["repo-mirroring-gitlab-ee#3959@2x/layers.json.js"] = [
	{
		"objectId": "2C75086A-682C-4C23-B759-3AF9FE51650A",
		"kind": "artboard",
		"name": "view1",
		"originalName": "view1",
		"maskFrame": null,
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 1500,
			"height": 2275
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"image": {
			"path": "images/Layer-view1-mkm3nta4.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 1500,
				"height": 2275
			}
		},
		"children": [
			{
				"objectId": "65A04D7B-A4A1-4F2E-A188-67F3BE612F1E",
				"kind": "group",
				"name": "breadcrumbs",
				"originalName": "breadcrumbs",
				"maskFrame": null,
				"layerFrame": {
					"x": 255,
					"y": 16,
					"width": 990,
					"height": 32
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-breadcrumbs-njvbmdre.png",
					"frame": {
						"x": 255,
						"y": 16,
						"width": 990,
						"height": 32
					}
				},
				"children": [
					{
						"objectId": "608E1E74-3C8C-47A7-A528-418E5A17A304",
						"kind": "group",
						"name": "Group",
						"originalName": "Group",
						"maskFrame": null,
						"layerFrame": {
							"x": 255,
							"y": 16,
							"width": 400,
							"height": 16
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group-nja4rtff.png",
							"frame": {
								"x": 255,
								"y": 16,
								"width": 400,
								"height": 16
							}
						},
						"children": []
					}
				]
			}
		]
	}
]