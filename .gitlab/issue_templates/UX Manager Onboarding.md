<!--

# The issue title should be: UX manager  onboarding (NAME), as (ROLE, STAGE GROUP)

-->

/assign (FILL IN WITH @ NEW TEAM MEMBER HANDLE) (FILL IN WITH @ UX BUDDY HANDLE)
/due (FILL IN WITH 1 MONTH AFTER START DATE)
/label ~onboarding
/confidential

## Hello  :wave:
Welcome to the UX Department!

We have created this issue to help you become immersed in your role as a UX Manager.

##### Getting questions answered
If you have questions, don't hesitate to @mention your UX buddy, @clenneville (your Manager), or the whole UX department by using `@gitlab\\-com/gitlab\\-ux` (use the latter sparingly unless it's something important). You can also reach out to people in the `#ux` Slack channel.

##### Onboarding expectations
Your onboarding doesn't have a scheduled end date. Just focus on getting comfortable with GitLab and getting to know your colleagues.

#### Company
GitLab is a special place with a unique culture. We document everything. Most of the information you will ever need is available in the GitLab [handbook](https://about.gitlab.com/handbook/). We view it as our single source of truth (SSOT).

###### Company structure
* [ ] First, it's likely that you want to know where your place is in the company structure. For this, you can check out the [GitLab org chart](https://about.gitlab.com/team/chart/). This is a good page to bookmark.
* [ ] The company uses [OKRs](https://about.gitlab.com/company/okrs/) to set goals and track progress. UX OKRs are listed under **UX Director**.

###### Getting started
* [ ] Understanding GitLab's [values](https://about.gitlab.com/handbook/values/) is really important. Our values describe the type of behavior that we expect from the people that we hire. They help us to know how to behave in the organization and what to expect from others. Values are a framework for distributed decision making, they allow you to determine what to do without asking your manager.
* [ ] [Learn how we communicate as a company](https://about.gitlab.com/handbook/communication/). We use asynchronous communication as a start and are as open as we can be by communicating through public issues, chat channels, and placing an emphasis on ensuring that conclusions of offline conversations are written down.
* [ ] When setting up your home office, get advice and guidance about what you might need and what you can expense by checking out the [spending company money page](https://about.gitlab.com/handbook/spending-company-money/).
* [ ] Finally, the [general guidelines](https://about.gitlab.com/handbook/general-guidelines/) will help you understand how we get things done.

###### Vacation
We have a "[no ask, must tell" policy](https://about.gitlab.com/handbook/paid-time-off/). You do not need to ask permission to take time off, unless you want to have more than 25 consecutive calendar days off. The 25-day no ask limit is per vacation, not per year.
If you are planning to be on vacation for a day or longer, please communicate your time off by:
* Updating [PTO Ninja](https://about.gitlab.com/handbook/paid-time-off/#pto-ninja) in Slack and changing your Slack status.
* Updating your [status](https://docs.gitlab.com/ee/user/profile/#current-status) in GitLab.
* Updating the [UX calendar](https://calendar.google.com/calendar?cid=Z2l0bGFiLmNvbV85cHNoMjZmaGEzZTRtdmhscmVmdXNiNjE5a0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t).
* Updating your personal calendar. If you use Google's `Out of Office` event feature, any meetings that are scheduled for when you are out of office will be auto-declined.
* Notifying Christie in your 1:1 meeting.

If you need any help with PTO Ninja you can post to [this issue](https://gitlab.com/gitlab-com/people-ops/General/issues/285#note_185466444).

You should communicate your time off for public holidays (since they differ depending on where you are in the world!).
You don't need to worry about taking an hour or two off to go to the gym, take a nap, go grocery shopping, do household chores, etc.

The same applies to your direct reports. They should use the above methods when taking time off. In addition, they should notify you in your 1:1 meeting.

#### UX Department
Each department has its own handbook section, and UX is no exception:
* [ ] Read through the whole [UX handbook](https://about.gitlab.com/handbook/engineering/ux/) section, please! Consider this your first team task, understanding the Mission, Vision, and how work is executed within the department. If you find any typos or items that could be made clearer, please consider [editing that page](https://gitlab.com/-/ide/project/gitlab-com/www-gitlab-com/edit/master/-/source/handbook/engineering/ux/index.html.md) by submitting a merge request.
* [ ] Read about [Pajamas](https://design.gitlab.com/), our design system.
* [ ] [UX Experience Factor Worksheets](https://drive.google.com/open?id=1KvhJPbsb2jE2MrYq46iEfncQR5FCI_gg) are how an employee's performance is mapped to their job description. It's helpful to get familiar with these.

##### Getting to know people
* [ ] Schedule weekly 1:1s with your direct reports. Most of your directs will have an existing agenda in Google docs that they used with their previous manager. Request access to this document and attach it to the 1:1 invite. The agenda is a great place for each of you to add items for discussion and to take notes on any decisions or action items.
* [ ] Schedule coffee chats with other members of the UX department to get to know who you work with. Talk about everyday things. We want you to make friends and build relationships with the people you work with to create a more comfortable, well-rounded environment.
* [ ] Schedule regular coffee chats with your Product Manager and Engineering Manager partners. Introduce yourself and get to know each other, since they will be some of your key contacts.
* [ ] If you want to bring something to the attention of all UX Department members, you can ping `@uxers` on Slack, email: `ux-department@gitlab.com` or mention `@gitlab\-com/gitlab\-ux` within GitLab (please use all forms sparingly unless it's something important).

#### Sharing a little about yourself
* [ ] GitLabbers use README.md files to share information about themselves. It’s another way to get to know someone, how they work, and what they enjoy. Create your own personal README.md. It’s up to you what you wish to share. For inspiration, take a look at the [UX Team's README's](https://about.gitlab.com/handbook/engineering/ux/#about-our-team).

##### Keeping track of everything
* [ ] Almost everything we do is within GitLab and it can sometimes be a challenge keeping up with everything. We use the [UX Management Board](https://gitlab.com/gitlab-org/gitlab-design/-/boards/1077469?label_name[]=UX%20Management) in the GitLab Design project as a helpful tool when collaborating with other UX Managers, tracking quarterly OKR progress, or proposing changes that may impact the entire UX department.
* [ ] You also have a ["To Do" list](https://gitlab.com/dashboard/todos?nav_source=navbar). Issues you are assigned to or mentioned on will show up here until you mark them as done.

###### Staying informed
* [ ] There are over 1000 Slack channels at GitLab. You can join as many as you like! However, you may find the following channels most useful at first: `#git-help`, `#peopleops`, `#product`, `#questions`, and `#ux`. Each stage group also has a Slack channel. Each stage group channel normally starts with the letter 'g'. For example, the Secure stage group channel is `#g_secure`. Join `#managers` for manager-related topics and announcements.
* [ ] You should have now received an invite for the `Company call` (every day except Friday). If you can't attend the call, don't worry, just check the [company call agenda](https://docs.google.com/document/d/1JiLWsTOm0yprPVIW9W-hM4iUsRxkBt_1bpm3VXV4Muc/edit) to stay on top of the most important events.
* [ ] There is usually a [group conversation](https://about.gitlab.com/handbook/people-operations/group-conversations/) before each company call. Group Conversations are a way for departments and stage groups to keep the wider business informed about what they are currently working on.
* [ ] We have a [UX Weekly Meeting](https://docs.google.com/document/d/189WZO7uTlZCznzae2gqLqFn55koNl3-pHvU-eVnvG9c/edit) every Tuesday.
* [ ] We have a weekly [UX Leadership meeting](https://docs.google.com/document/d/189WZO7uTlZCznzae2gqLqFn55koNl3-pHvU-eVnvG9c/edit) every Monday.
* [ ] We have an early/late `UX Hangout` call twice a week. This is where members of the UX Department can meet informally to talk about anything they want.
* [ ] There is a monthly call between UX and the Frontend Department.
* [ ] We also have an early/late [Group Design Review](https://docs.google.com/document/d/1-t7qcGk24ev8P6_Ktpyfsx_cNgnve6YnryQiL49407Q/edit?usp=sharing) twice a week. This is a session where designers can share what they are working on with their colleagues and get feedback on their designs. Managers should try to make this a priority.
* [ ] You will have a weekly 1:1 meeting with Christie. This meeting is for you! Please take a moment before your 1:1s to add items to the agenda. This can be anything from an update on what you are working on to questions and concerns about the team, company, etc. This is also where you'll review progress on issues assigned in the [UX Management Board](https://gitlab.com/gitlab-org/gitlab-design/-/boards/1077469?label_name[]=UX%20Management).
* [ ] Christie is here to support you! If you need a 1:1 chat at any time, book some time on her calendar. You can also DM her on Slack via `@clenneville`.
* [ ] You are not obligated to attend any of the meetings that you are invited to. However, please notify others of your absence by responding to Google calendar invites. If you do not attend a meeting, please read the agenda items and go through the notes of the meeting. Also, check whether there is a recording available.
* [ ] Recordings can be found on [GitLab Unfiltered Channel](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A), [GitLab Channel](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg), and [Google Drive](https://drive.google.com/drive/folders/1FLKOVDqiD7lMn11gvaVE8P4wWPf3FwFM). The meetings in Google Drive are organized in folders by meeting name.

##### 2019 Engagement Survey Next Steps
GitLab runs regular [Employee Engagement Surveys](https://about.gitlab.com/handbook/people-group/engagement/) to reduce attrition, improve productivity and efficiency, enhance company values, and increase job satisfaction. After the survey results are shared, each department determines Next Steps based on feedback.

As part of your regular workflow, please remember to include the Next Steps from the 2019 survey while managing your team:

* Use the #thanks channel frequently in Slack to acknowledge great work. Encourage your team to do the same.
* Encourage your team to use the UX Weekly Call as a Knowledge Sharing opportunity. For example, if they have an area of particular expertise or if they learn something new and interesting, ask them to add it to an upcoming agenda.
* Make sure that everyone on your team has an [Individual Growth Plan](https://docs.google.com/document/d/1ZjdIuK5mNpljiHnFMK4dvqfTOzV9iSJj66OtoYbniFM/edit?usp=sharing) (IGP) that includes personal development items. Check in on the IGP monthly to help support your team's growth.
* Make time in your schedule for your own personal growth, and share your efforts with your team. This is a great way to show the importance of these activities.
* Make sure your team knows about our [conference budget](https://about.gitlab.com/handbook/spending-company-money/#work-related-online-courses-and-professional-development-certifications) and how to take advantage of it.

##### Your onboarding experience
This issue is confidential by default, meaning this is a safe space to ask any questions and to describe any experiences you might have had. It's a tool to help **you** succeed!

Please share your newfound knowledge! The rest of the UX Department would love to hear your experiences. You are in a position to help make things better for everyone (colleagues, new teammates, etc), including you.

Within this issue, please let us know how we can improve the onboarding experience. Please keep notes on what went well, what didn't go well, or what you feel could be improved. We'll use your feedback to improve the onboarding experience for future people who join the team.

* [ ] If your Manager hasn't done so on your behalf, please add yourself to the next [UX weekly meeting agenda](https://docs.google.com/document/d/189WZO7uTlZCznzae2gqLqFn55koNl3-pHvU-eVnvG9c/edit?usp=sharing) and spend 3-5 minutes max in total sharing a summary of your experience with the team.

#### Closing thoughts
Taking in a lot of information can quickly feel overwhelming. This is normal and you are encouraged to ask questions at any time. Embrace knowing nothing and you'll take it in as you go. Take your time to work through your onboarding tasks; you are not expected to be at 100% productivity right away! And remember: if you are here, it's because you are the best person we have found in the world (literally) to do this job.

Working remotely comes with great power and great responsibility. A member of the UX team wrote a [helpful article on being efficient and productive](https://about.gitlab.com/2018/05/17/eliminating-distractions-and-getting-things-done/).

##### Remember to practice self-care
Make sure you take breaks if you feel overwhelmed or tired, especially if this is your first remote job. Remember to stretch your legs, drink some water, and disconnect when necessary. Try to establish a healthy routine that will empower you to work in a way that enables you to deliver the best results.
