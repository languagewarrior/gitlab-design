<!--
 
Title should be: Category Maturity Scorecard - {{Stage Group}} FY{{YY}}-Q{{quarter number}} - {{Title or Description of the Evaluated Workflow / JTBD}}
(e.g. “Category Maturity Scorecard - Create:Source Code FY21-Q1 - Obtaining screenshots from testing artifacts)

This template is meant to document results from Category Maturity Scorecard user interview sessions.

If this CM Scorecard is related to an OKR, append ~OKR to the /label quick action below to automatically add the 'OKR' label.

-->

/label ~"CM scorecard" 

- **Research issue**: {{add link to research issue for the CM Scorecard research process}}
- **Previous score and scorecard**: {{if applicable, add CM Scorecard score and link to scorecard issue}}
- **Walkthrough**: {{add link to YouTube video or walkthrough document}}
- **Recommendations**: {{add link to your recommendation issue/s}}

# Category Maturity Scorecard Checklist

[Learn more about Category Maturity Scorecards](https://about.gitlab.com/handbook/engineering/ux/category-maturity-scorecards/)
1. [ ] Review the Category Maturity Scorecard handbook page and follow the process as described. Reach out to the [UX Researcher for your stage](https://about.gitlab.com/handbook/product/categories/) if you have questions.
1. [ ] Document the results of each JTBD scenario using this template
1. [ ] Add links for each participants' session recordings. Consider downloading the session videos from Zoom, and uploading them to the shared Google UX Research folder in the appropriate section/stage/project folder.
1. [ ] If the participant has not granted permission to share the recording publicly, ensure the sharing settings are set to GitLab-only.
1. [ ] If needed, [create a recommendation issue](https://gitlab.com/gitlab-org/gitlab-design/issues/new?issuable_template=UX%20Scorecard%20Part%202) for these sessions.

# CM Scorecard Results Template
### Job to be done A
{{add JTBD here}}

### Scenarios
##### Scenario 1 
* Prompt - {{add scenario prompt here}}
* Average UMUX Lite score for capabilities - {{score}}
* Average UMUX Lite score for ease of use - {{score}}
* How many participants were successful at the task - {{number of successful participants/out of total participants}}
* How many participants failed the task - {{number of participants who failed/out of total participants}}
* Total number of errors each participant encountered while attempting to complete the task/scenario - {{number of errors}}

| Participant Number | Successful  | Failed  | Number of Errors Encountered  |
|:-:|---|---|---|
| P1  |   |   |   |
| P2  |   |   |   |
| P3  |   |   |   |
| P4  |   |   |   |
| P5  |   |   |   |


##### Scenario 2
* Prompt - {{add scenario prompt here}}
* Average UMUX Lite score for capabilities - {{score}}
* Average UMUX Lite score for ease of use - {{score}}
* How many participants were successful at the task - {{number of successful participants/out of total participants}}
* How many participants failed the task - {{number of participants who failed/out of total participants}}
* Total number of errors each participant encountered while attempting to complete the task/scenario - {{number of errors}}

| Participant Number | Successful  | Failed  | Number of Errors Encountered  |
|:-:|---|---|---|
| P1  |   |   |   |
| P2  |   |   |   |
| P3  |   |   |   |
| P4  |   |   |   |
| P5  |   |   |   |

{{continue with as many scenarios as you used}}

### Participants 
##### Participant 1
 * {{Role}}
 * {{Top 3 tasks/responsibilities}}
 * {{Previous/current GitLab usage}}
 * {{add link to session video}}

##### Participant 2
 * {{Role}}
 * {{Top 3 tasks/responsibilities}}
 * {{Previous/current GitLab usage}}
 * {{add link to session video}}

 {{continue with as many participants as you had in the study}}


#### Other notes
{{Add any additional notes that came from freeform discussion or elsewhere}}
